
window.onload = init;

guessX = 0; //stores user's click on canvas
guessY = 0; //stores user's click on canvas
var cX=0;
var cY=0;
var colores = ["white","orange","yellow","blue","green"];

var coordenadas = { 1:{1:{1:0,2:0},2:{1:120,2:0},3:{1:240,2:0},4:{1:360,2:0}},
                    2:{1:{1:0,2:80},2:{1:120,2:80},3:{1:240,2:80},4:{1:360,2:80}},
                    3:{1:{1:0,2:160},2:{1:120,2:160},3:{1:240,2:160},4:{1:360,2:160}},
                    4:{1:{1:0,2:240},2:{1:120,2:240},3:{1:240,2:240},4:{1:360,2:240}}
                    }

var matrix = { 1:{1:0,2:1,3:0,4:1},
               2:{1:2,2:0,3:2,4:0},
               3:{1:0,2:3,3:0,4:3},
               4:{1:4,2:0,3:4,4:0}
            }


function storeGuess(event){
    var x = event.offsetX;
    var y = event.offsetY;
    guessX = x;
    guessY = y;
    //console.log("x coords: " + guessX + ", y coords: " + guessY);
    gX=guessX;
    gY=guessY;
}

 function init(){
    var canvas = document.getElementById("canvas1");
    var ctx = canvas.getContext("2d");
    ctx.strokeStyle = colores[0];
	ctx.lineWidth = 1;
	ctx.strokeRect(0, 0, 120, 80);
	ctx.fillStyle = colores[1];
    ctx.fillRect(120,0,120,80);
    ctx.strokeStyle = colores[0];
	ctx.lineWidth = 1;
	ctx.strokeRect(240, 0, 120, 80);
	ctx.fillStyle = colores[1];
    ctx.fillRect(360,0,120,80);
    ctx.fillStyle = colores[2];
    ctx.fillRect(0,80,120,80);
    ctx.strokeStyle = colores[0];
	ctx.lineWidth = 1;
	ctx.strokeRect(120, 80, 120, 80);
	ctx.fillStyle = colores[2];
	ctx.fillRect(240,80,120,80);
    ctx.strokeStyle = colores[0];
    ctx.lineWidth = 1;
	ctx.strokeRect(360, 80, 120, 80);
	ctx.strokeStyle = colores[0];
	ctx.lineWidth = 1;
	ctx.strokeRect(0, 160, 120, 80);
	ctx.fillStyle = colores[3];
    ctx.fillRect(120,160,120,80);
    ctx.strokeStyle = colores[0];
	ctx.lineWidth = 1;
	ctx.strokeRect(240, 160, 120, 80);
	ctx.fillStyle = colores[3];
    ctx.fillRect(360,160,120,80);
    ctx.fillStyle = colores[4];
    ctx.fillRect(0,240,120,80);
    ctx.strokeStyle = colores[0];
	ctx.lineWidth = 1;
	ctx.strokeRect(120, 240, 120, 80);
	ctx.fillStyle = colores[4];
	ctx.fillRect(240,240,120,80);
    ctx.strokeStyle = colores[0];
    ctx.lineWidth = 1;
    ctx.strokeRect(360, 240, 120, 80);
}

function cambioHorizontal(){
    console.log(gX + " ---- "+gY);
    var coordenadax = 0;
    var coordenaday = 0;

    coordenadax = getCoordenadaX(gX);
    coordenaday = getCoordenadaY(gY);
    
    var fila = getFila(coordenaday);
    var columna = getColumna(coordenadax);

    
   

    console.log(matrix[fila][columna]);
  
   
    tipoCambio(coordenadax,coordenaday,"horizontal",fila,columna);
    



   

}


function tipoCambio(coordenadax,coordenaday,tipo,fila,columna){
   
    
    if(tipo=="horizontal"){

        if(matrix[fila][columna]==0){
            if(matrix[fila][columna+1]==fila || matrix[fila][columna-1]==fila ){
                intercambioHorizontal(coordenadax,coordenaday,fila,columna);
            }
            else{
                alert("No es posible ese movimiento");
            }
           
        }
        else{
            alert("Error solo se pueden cambiar espacios en Blanco!");
        }

    }
    else if(tipo = "vertical"){

    }
    else{
        alert("Error no se puede establecer el valor !");
    }

}

function intercambioHorizontal(coordenadax,coordenaday,fila,columna){
    var canvas = document.getElementById("canvas1");
    var ctx = canvas.getContext("2d");
    matrix[fila][columna]=fila;
    ctx.fillStyle = getColor(fila);
    ctx.fillRect(coordenadax,coordenaday,120,80);
    
    if(matrix[fila][columna+1]==fila){
        matrix[fila][columna+1]=0;
        ctx.fillStyle = "white"
        ctx.lineWidth = 1;
        ctx.fillRect(coordenadas[fila][columna+1][1], coordenadas[fila][columna+1][2], 120, 80);
    }
    else{
        matrix[fila][columna-1]=0;
        ctx.fillStyle = "white"
        ctx.lineWidth = 1;
        ctx.fillRect(coordenadas[fila][columna-1][1], coordenadas[fila][columna-1][2], 120, 80);
    }
}




function cambioVertical(){

}

function getColor(num){
    switch (num) {
        case 0:
            return "white";
            break;
        case 1:
            return "orange";
            break;
        case 2:
            return "yellow";
            break;
        case 3:
            return "blue";
            break;
        case 4:
            return "green";
            break;
        default:
            break;
    }
}

function getCoordenadaX(x){
    if(x<120){
        return 0;
    }
    else if(x>=120 && x<240){
        return 120;
    }
    else if(x>=240 && x<360){
        return 240;
    }
    else{
        return 360;
    }
}

function getCoordenadaY(y){
    if(y<80){
        return 0;
    }
    else if(y>=80 && y<160){
        return 80;
    }
    else if(y>=160 && y<240){
        return 160;
    }
    else{
        return 240;
    }
}


function getFila(coordenaday){
    if(coordenaday<80){
        return 1;
    }
    else if(coordenaday>=80 && coordenaday<160){
        return 2;
    }
    else if(coordenaday>=160 && coordenaday<240){
        return 3;
    }
    else{
        return 4;
    }
}

function getColumna(coordenadax){
    if(coordenadax<120){
        return 1;
    }
    else if(coordenadax>=120 && coordenadax<240){
        return 2;
    }
    else if(coordenadax>=140 && coordenadax<360){
        return 3;
    }
    else{
        return 4;
    }
 }